﻿namespace reapet.Services
{
    public class RepeatingServiceOne : BackgroundService
    {
        private ILogger<RepeatingServiceOne> logger;
        public RepeatingServiceOne(ILogger<RepeatingServiceOne> logger)
        {
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await DoWork();
                await Task.Delay(1000, stoppingToken);
            }
        }

        private async Task DoWork()
        {
            Console.WriteLine(DateTime.Now.ToString("o"));
        }
    }
}
