﻿namespace reapet.Services
{
    public class RepeatingServiceTwo : BackgroundService
    {
        private readonly PeriodicTimer _timer = new(TimeSpan.FromMinutes(1));
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (await _timer.WaitForNextTickAsync(stoppingToken) && !stoppingToken.IsCancellationRequested)
            {
                await ReadFiles();
            }
        }

        private async Task ReadFiles()
        {
            Console.WriteLine($"reading files {DateTime.Now.ToString("o")}");
            await Task.Delay(1000);
        }
    }
}
